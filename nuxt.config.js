module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'Gitlab Grammar Rodeo',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Not Understood Except Tardily' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: '//fonts.googleapis.com/css?family=News+Cycle|Material+Icons' },
      { rel: 'stylesheet', href: 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.min.css' }
    ]
  },
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#6699cc' },
// build: {
//     /*
//     ** You can extend webpack config here
//     */
//     extend(config, ctx) {
//
//     }
// },
//
generate: {
    dir: 'public',
	},
		router: {
			base: '/vuematerial-grammar-school'
		},
// 	}
  /*
  ** Build configuration
  */
  css: [
    { src: 'vue-material/dist/vue-material.min.css', lang: 'css' },
    { src: '~/assets/theme.scss', lang: 'scss' },
    { src: '~/assets/overrides.scss', lang: 'scss' }
  ],
  plugins: [
    { src: '~/plugins/vue-material' }
  ],
  build: {
    /*
    ** Run ESLint on save
    */
    vendor: ['vue-material'],
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}
